<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuManageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_manage', function (Blueprint $table) {
            $table->increments('me_id');
            $table->string('menu_name');
            $table->string('menu_heading');
            $table->string('menu_img');
            $table->string('img_align');
            $table->string('menu_desc');
            $table->string('status');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_manage');
    }
}
