<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideManageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slide_manage', function (Blueprint $table) {
            $table->increments('sl_id');
            $table->string('heading');
            $table->string('sub_heading');
            $table->string('slide_img');
            $table->string('status');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slide_manage');
    }
}
