<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicManageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_manage', function (Blueprint $table) {
            $table->increments('ba_id');
            $table->string('title');
            $table->string('web_name');
            $table->string('copy_name');
            $table->string('logo');
            $table->string('phone');
            $table->string('email');
            $table->string('copy_link');
            $table->string('copy_year');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('basic_manage');
    }
}
