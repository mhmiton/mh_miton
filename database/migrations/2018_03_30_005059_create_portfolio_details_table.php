<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_details', function (Blueprint $table) {
            $table->increments('de_id');
            $table->integer('port_cat_id');
            $table->string('project_name');
            $table->string('project_url');
            $table->string('client');
            $table->date('date');
            $table->string('date_str');
            $table->string('project_img');
            $table->longtext('project_desc');
            $table->string('status');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_details');
    }
}
