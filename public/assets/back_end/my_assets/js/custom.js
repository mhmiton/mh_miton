$(document).ready(function(){

	$('.del_btn').click(function(){
		var url = $(this).data('href');
	    var id 	= $(this).data('id');
	    swal({title:'Are You Sure Delete This Data', icon:'warning', buttons:true, dangerMode:true})
	    .then((willDelete) => {
	      if(willDelete)
	      {
	        location.replace('/'+url+'_clear/'+id);
	      }
	    });
	});

    $('.modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

});

// function del(attr)
// {
//     var url = attr.data('href');
//     var id = attr.data('id');
//     swal({title:'Are You Sure Delete This Data', icon:'warning', buttons:true, dangerMode:true})
//     .then((willDelete) => {
//       if(willDelete)
//       {
//         location.replace('/'+url+'_clear/'+id);
//       }
//     });
// }

function reset(form)
{
    $(form)[0].reset();
    $('#submit').text('Save');
    $('#tagsinput').tagsinput('removeAll');
    CKEDITOR.instances.editor.setData('');
}

function reset_file()
{
    $('.file_name').text('');
    $('.file').prop('required',true);
}

function img_valid(attr)
{
    var file    = attr.val().toLowerCase();
    var ext     = /(\.jpg|\.jpeg|\.gif|\.png)$/i;
    if(ext.test(file))
    {
        return true;
    } else {
        attr.val('');
        swal({
            title: 'Please Inter A Valid Image File',
            icon: 'warning',
            button: true
        });
        return false;
    }
}

function oninput_num(attr)
{
    var num = Math.abs(attr.val(),2);
    attr.val(num);
}