<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class My_join extends Model
{
    public static function join_portfolio()
    {
    	$query = DB::table('portfolio_details');
    	$query->join('portfolio_category','portfolio_details.port_cat_id','=','portfolio_category.port_id');
    	$query->select('portfolio_details.*','portfolio_category.port_category');
    	return $query->get();
    }
}
