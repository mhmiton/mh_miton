<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;

class Web_page extends Controller
{
    public function view_page()
    {
    	$data['basic_data']	= My_model::get_one_row('basic_manage','','','');
    	$data['menu_data']	= My_model::get_all_row('menu_manage',['status'=>'Active'],'','');
    	$data['slide_data']	= My_model::get_one_row('slide_manage',['status'=>'Active'],'sl_id,desc','0,1');
    	$data['about_data']	= My_model::get_one_row('about_manage',['status'=>'Active'],'ab_id,desc','0,1');
    	$data['skills_data']= My_model::get_all_row('skills_manage',['status'=>'Active'],'','');
    	$data['service_data']= My_model::get_all_row('service_manage',['status'=>'Active'],'','');
    	$data['port_category']= My_model::get_all_row('portfolio_category',['status'=>'Active'],'','');
    	$data['stat_data']= My_model::get_all_row('stat_manage',['status'=>'Active'],'','');
    	$data['test_data']= My_model::get_all_row('testimonial_manage',['status'=>'Active'],'','');
    	$data['contact_data']= My_model::get_all_row('contact_manage',['status'=>'Active'],'','');
    	$data['social_data']= My_model::get_all_row('social',['status'=>'Active'],'','');

    	return view('web.index',$data);
    }

    public function contact_action(Request $request)
    {
        $basic_data =  My_model::get_one_row('basic_manage','','','');

        $data['name']       = $request->name;
        $data['email']      = $request->email;
        $data['subject']    = $request->subject;
        $data['message']    = $request->message;

        $to                 = $basic_data->email;
        $sub                = $data['subject'];
        $mess               = $data['message'];
        $headers            = "From:".$data['email'];

        mail($to,$sub,$mess,$headers);

        $alert['type']      = 'success';
        $alert['message']   = 'Your Message Sent Successfull';
        return json_encode($alert);
    }
}
