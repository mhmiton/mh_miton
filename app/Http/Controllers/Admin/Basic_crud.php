<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Basic_crud extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $data['title']             = $request->title;
        $data['web_name']          = $request->web_name;
        $data['phone']             = $request->phone;
        $data['email']             = $request->email;
        $data['copy_name']         = $request->copy_name;
        $data['copy_link']         = $request->copy_link;
        $data['copy_year']         = $request->copy_year;
        $id                        = $request->id;

        $img_field = ['logo'];
        foreach ($img_field as $key => $img)
        {
            if($request->hasFile($img))
            {
                $file = $request->file($img);
                $name = time()+($key+1).'.'.$file->getClientOriginalExtension();
                $path = public_path('upload/logo/');
                Image::make($file)->resize('150','150')->save($path.$name);
                $data[$img] = $name;
                if($id) { My_model::delete_img('basic_manage',['ba_id'=>$id],$path,$img); }

            }
        }

        if(!$id)
        {
            $save     = My_model::insert('basic_manage',$data);
            session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
        } else {
            $update   = My_model::data_update('basic_manage',['ba_id'=>$id],$data);
            session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
        }

        return redirect('/pages/basic_manage/basic_data/ba');
    }

    public function edit(Request $request)
    {
        $id    = $request->id;
        $data  = My_model::get_one_row('basic_manage',['ba_id'=>$id],'','');
        return response()->json($data);
    }

    public function delete($id)
    {
        $img_field  = ['logo'];
        foreach ($img_field as $key => $img)
        {
            $path   = public_path('upload/logo/');
            My_model::delete_img('basic_manage',['ba_id'=>$id],$path,$img);
        }

        $delete     = My_model::data_delete('basic_manage',['ba_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
        return redirect('/pages/basic_manage/basic_data/ba');
    }

}
