<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Slide_crud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
    	$data['heading']		    = $request->heading;
    	$data['sub_heading']  	    = $request->sub_heading;
    	$data['status']            	= $request->status;
    	$id                        	= $request->id;

        $img_field = ['slide_img'];
        foreach ($img_field as $key => $img)
        {
            if($request->hasFile($img))
            {
                $file = $request->file($img);
                $name = time()+($key+1).'.'.$file->getClientOriginalExtension();
                $path = public_path('upload/slider/');
                Image::make($file)->resize('900','900')->save($path.$name);
                $data[$img] = $name;
                if($id) { My_model::delete_img('slide_manage',['sl_id'=>$id],$path,$img); }

            }
        }

    	if(!$id)
    	{
	    	$save     = My_model::insert('slide_manage',$data);
	    	session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
	    } else {
	    	$update   = My_model::data_update('slide_manage',['sl_id'=>$id],$data);
	    	session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
	    }

    	return redirect('/pages/slide_manage/slide_data/sl');
    }

    public function edit(Request $request)
    {
    	$id    = $request->id;
    	$data  = My_model::get_one_row('slide_manage',['sl_id'=>$id],'','');
    	return response()->json($data);
    }

    public function delete($id)
    {
        $img_field  = ['slide_img'];
        foreach ($img_field as $key => $img)
        {
            $path   = public_path('upload/slider/');
            My_model::delete_img('slide_manage',['sl_id'=>$id],$path,$img);
        }

    	$delete     = My_model::data_delete('slide_manage',['sl_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
    	return redirect('/pages/slide_manage/slide_data/sl');
    }

}

