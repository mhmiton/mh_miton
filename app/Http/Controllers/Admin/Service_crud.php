<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Service_crud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
    	$data['service_name']		= $request->service_name;
    	$data['service_features']  	= $request->service_features;
    	$data['service_desc']      	= $request->service_desc;
    	$data['status']            	= $request->status;
    	$id                        	= $request->id;

        $img_field = ['service_img'];
        foreach ($img_field as $key => $img)
        {
            if($request->hasFile($img))
            {
                $file = $request->file($img);
                $name = time()+($key+1).'.'.$file->getClientOriginalExtension();
                $path = public_path('upload/service/');
                Image::make($file)->resize('150','150')->save($path.$name);
                $data[$img] = $name;
                if($id) { My_model::delete_img('service_manage',['se_id'=>$id],$path,$img); }

            }
        }

    	if(!$id)
    	{
	    	$save     = My_model::insert('service_manage',$data);
	    	session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
	    } else {
	    	$update   = My_model::data_update('service_manage',['se_id'=>$id],$data);
	    	session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
	    }

    	return redirect('/pages/service_manage/service_data/se');
    }

    public function edit(Request $request)
    {
    	$id    = $request->id;
    	$data  = My_model::get_one_row('service_manage',['se_id'=>$id],'','');
    	return response()->json($data);
    }

    public function delete($id)
    {
        $img_field  = ['service_img'];
        foreach ($img_field as $key => $img)
        {
            $path   = public_path('upload/service/');
            My_model::delete_img('service_manage',['se_id'=>$id],$path,$img);
        }

    	$delete     = My_model::data_delete('service_manage',['se_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
    	return redirect('/pages/service_manage/service_data/se');
    }

}

