<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Testimonial_crud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
    	$data['name']				= $request->name;
    	$data['designation']  		= $request->designation;
    	$data['description']      	= $request->description;
    	$data['status']            	= $request->status;
    	$id                        	= $request->id;

        $img_field = ['image'];
        foreach ($img_field as $key => $img)
        {
            if($request->hasFile($img))
            {
                $file = $request->file($img);
                $name = time()+($key+1).'.'.$file->getClientOriginalExtension();
                $path = public_path('upload/testimonial/');
                Image::make($file)->resize('150','150')->save($path.$name);
                $data[$img] = $name;
                if($id) { My_model::delete_img('testimonial_manage',['te_id'=>$id],$path,$img); }

            }
        }

    	if(!$id)
    	{
	    	$save     = My_model::insert('testimonial_manage',$data);
	    	session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
	    } else {
	    	$update   = My_model::data_update('testimonial_manage',['te_id'=>$id],$data);
	    	session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
	    }

    	return redirect('/pages/testimonial_manage/testimonial_data/te');
    }

    public function edit(Request $request)
    {
    	$id    = $request->id;
    	$data  = My_model::get_one_row('testimonial_manage',['te_id'=>$id],'','');
    	return response()->json($data);
    }

    public function delete($id)
    {
        $img_field  = ['image'];
        foreach ($img_field as $key => $img)
        {
            $path   = public_path('upload/testimonial/');
            My_model::delete_img('testimonial_manage',['te_id'=>$id],$path,$img);
        }

    	$delete     = My_model::data_delete('testimonial_manage',['te_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
    	return redirect('/pages/testimonial_manage/testimonial_data/te');
    }

}

