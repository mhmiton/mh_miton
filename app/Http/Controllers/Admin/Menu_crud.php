<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Menu_crud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $data['menu_name']         = $request->menu_name;
        $data['menu_heading']      = $request->menu_heading;
    	$data['img_align']         = $request->img_align;
    	$data['menu_desc']         = $request->menu_desc;
    	$data['status']            = $request->status;
    	$id                        = $request->id;

        $img_field = ['menu_img'];
        foreach ($img_field as $key => $img)
        {
            if($request->hasFile($img))
            {
                $file = $request->file($img);
                $name = time()+($key+1).'.'.$file->getClientOriginalExtension();
                $path = public_path('upload/menu/');
                Image::make($file)->resize('800','600')->save($path.$name);
                $data[$img] = $name;
                if($id) { My_model::delete_img('menu_manage',['me_id'=>$id],$path,$img); }

            }
        }

    	if(!$id)
    	{
	    	$save     = My_model::insert('menu_manage',$data);
	    	session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
	    } else {
	    	$update   = My_model::data_update('menu_manage',['me_id'=>$id],$data);
	    	session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
	    }

    	return redirect('/pages/menu_manage/menu_data/me');
    }

    public function edit(Request $request)
    {
    	$id    = $request->id;
    	$data  = My_model::get_one_row('menu_manage',['me_id'=>$id],'','');
    	return response()->json($data);
    }

    public function delete($id)
    {
        $img_field  = ['menu_img'];
        foreach ($img_field as $key => $img)
        {
            $path   = public_path('upload/menu/');
            My_model::delete_img('menu_manage',['me_id'=>$id],$path,$img);
        }

    	$delete     = My_model::data_delete('menu_manage',['me_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
    	return redirect('/pages/menu_manage/menu_data/me');
    }

}
