<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Social_crud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $data['icon']              = $request->icon;
        $data['url']           = $request->url;
        $data['status']            = $request->status;
        $id                        = $request->id;

        if(!$id)
        {
            $save     = My_model::insert('social',$data);
            session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
        } else {
            $update   = My_model::data_update('social',['so_id'=>$id],$data);
            session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
        }

        return redirect('/pages/social/social_data/so');
    }

    public function edit(Request $request)
    {
        $id    = $request->id;
        $data  = My_model::get_one_row('social',['so_id'=>$id],'','');
        return response()->json($data);
    }

    public function delete($id)
    {
        $delete     = My_model::data_delete('social',['so_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
        return redirect('/pages/social/social_data/so');
    }

}
