<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Portfolio_details_crud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $date                      = explode('-',$request->date);
        $data['port_cat_id']       = $request->port_cat_id;
        $data['project_name']      = $request->project_name;
        $data['project_url']       = $request->project_url;
        $data['client']            = $request->client;
        $data['date']              = $date[2].'-'.$date[1].'-'.$date[0];
        $data['date_str']          = date("d F Y",strtotime($request->date));
        $data['project_desc']      = $request->project_desc;
        $data['status']            = $request->status;
        $id                        = $request->id;

        $img_field = ['project_img'];
        foreach ($img_field as $key => $img)
        {
            if($request->hasFile($img))
            {
                $file = $request->file($img);
                $name = time()+($key+1).'.'.$file->getClientOriginalExtension();
                $path = public_path('upload/portfolio/');
                Image::make($file)->resize('1280','850')->save($path.$name);
                $data[$img] = $name;
                if($id) { My_model::delete_img('portfolio_details',['de_id'=>$id],$path,$img); }

            }
        }

        if(!$id)
        {
            $save     = My_model::insert('portfolio_details',$data);
            session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
        } else {
            $update   = My_model::data_update('portfolio_details',['de_id'=>$id],$data);
            session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
        }

        return redirect('/pages/portfolio_details/details/de');
    }

    public function edit(Request $request)
    {
        $id    = $request->id;
        $data  = My_model::get_one_row('portfolio_details',['de_id'=>$id],'','');
        return response()->json($data);
    }

    public function delete($id)
    {
        $img_field  = ['project_img'];
        foreach ($img_field as $key => $img)
        {
            $path   = public_path('upload/portfolio/');
            My_model::delete_img('portfolio_details',['de_id'=>$id],$path,$img);
        }

        $delete     = My_model::data_delete('portfolio_details',['de_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
        return redirect('/pages/portfolio_details/details/de');
    }

}

