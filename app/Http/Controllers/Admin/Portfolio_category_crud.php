<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Portfolio_category_crud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $data['port_category']     = $request->port_category;
        $data['status']            = $request->status;
        $id                        = $request->id;

        if(!$id)
        {
            $save     = My_model::insert('portfolio_category',$data);
            session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
        } else {
            $update   = My_model::data_update('portfolio_category',['port_id'=>$id],$data);
            session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
        }

        return redirect('/pages/portfolio_category/port_category/port');
    }

    public function edit(Request $request)
    {
        $id    = $request->id;
        $data  = My_model::get_one_row('portfolio_category',['port_id'=>$id],'','');
        return response()->json($data);
    }

    public function delete($id)
    {
        $delete     = My_model::data_delete('portfolio_category',['port_id'=>$id]);
        $delete_2   = My_model::data_delete('portfolio_details',['port_cat_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
        return redirect('/pages/portfolio_category/port_category/port');
    }

}
