<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Skills_crud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $data['skills_name']       = $request->skills_name;
        $data['skills_count']      = $request->skills_count;
        $data['status']            = $request->status;
        $id                        = $request->id;

        if(!$id)
        {
            $save     = My_model::insert('skills_manage',$data);
            session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
        } else {
            $update   = My_model::data_update('skills_manage',['sk_id'=>$id],$data);
            session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
        }

        return redirect('/pages/skills_manage/skills_data/sk');
    }

    public function edit(Request $request)
    {
        $id    = $request->id;
        $data  = My_model::get_one_row('skills_manage',['sk_id'=>$id],'','');
        return response()->json($data);
    }

    public function delete($id)
    {
        $delete     = My_model::data_delete('skills_manage',['sk_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
        return redirect('/pages/skills_manage/skills_data/sk');
    }

}
