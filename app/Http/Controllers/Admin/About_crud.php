<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class About_crud extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
    	$data['about_heading']     = $request->about_heading;
    	$data['about_desc']        = $request->about_desc;
    	$data['status']            = $request->status;
    	$data['updated_at']        = \Carbon\Carbon::now();
    	$id                        = $request->id;

        $img_field = ['about_img'];
        foreach ($img_field as $key => $img)
        {
            if($request->hasFile($img))
            {
                $file = $request->file($img);
                $name = time()+($key+1).'.'.$file->getClientOriginalExtension();
                $path = public_path('upload/about/');
                Image::make($file)->resize('1500','1650')->save($path.$name);
                $data[$img] = $name;
                if($id) { My_model::delete_img('about_manage',['ab_id'=>$id],$path,$img); }

            }
        }

        // if($request->hasFile('about_img'))
        // {
        //     $about_img = $request->file('about_img');
        //     $img_name = time().'.'.$about_img->getClientOriginalExtension();
        //     $path = public_path('upload/about/'.$img_name);
        //     Image::make($about_img)->save($path);
        // }

    	if(!$id)
    	{
	    	$data['created_at'] = \Carbon\Carbon::now();
	    	$save     = My_model::insert('about_manage',$data);
	    	session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
	    } else {
	    	$update   = My_model::data_update('about_manage',['ab_id'=>$id],$data);
	    	session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
	    }

    	return redirect('/pages/about_manage/about_data/ab');
    }

    public function edit(Request $request)
    {
    	$id    = $request->id;
    	$data  = My_model::get_one_row('about_manage',['ab_id'=>$id],'','');
    	return response()->json($data);
    }

    public function delete($id)
    {
        $img_field  = ['about_img'];
        foreach ($img_field as $key => $img)
        {
            $path   = public_path('upload/about/');
            My_model::delete_img('about_manage',['ab_id'=>$id],$path,$img);
        }

    	$delete     = My_model::data_delete('about_manage',['ab_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
    	return redirect('/pages/about_manage/about_data/ab');
    }

}
