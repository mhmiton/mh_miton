<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\My_model;
use Image;

class Stat_crud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $data['icon']              = $request->icon;
        $data['title']             = $request->title;
        $data['count_number']      = $request->count_number;
        $data['status']            = $request->status;
        $id                        = $request->id;

        if(!$id)
        {
            $save     = My_model::insert('stat_manage',$data);
            session()->put(['alert'=>'Your Data Insert Successfull','type'=>'success']);
        } else {
            $update   = My_model::data_update('stat_manage',['st_id'=>$id],$data);
            session()->put(['alert'=>'Your Data Update Successfull','type'=>'success']);
        }

        return redirect('/pages/stat_manage/stat_data/st');
    }

    public function edit(Request $request)
    {
        $id    = $request->id;
        $data  = My_model::get_one_row('stat_manage',['st_id'=>$id],'','');
        return response()->json($data);
    }

    public function delete($id)
    {
        $delete     = My_model::data_delete('stat_manage',['st_id'=>$id]);
        session()->put(['alert'=>'Your Data Delete Successfull','type'=>'success']);
        return redirect('/pages/stat_manage/stat_data/st');
    }

}
