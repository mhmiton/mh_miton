<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\My_model;
use App\My_join;


class Admin_home extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
    	return view('admin/dashboard')->with('page_content',null);
    }

    public function pages($page_content,$db_data,$order)
    {
    	$result['page_content']    = $page_content;
        $result[$db_data]          = My_model::get_all_row($page_content,'',$order.'_id,desc','');

        if($page_content == 'portfolio_details')
        {
            $result['port_category']   = My_model::get_all_row('portfolio_category',['status'=>'Active'],'port_id,asc','');
            $result['port_details']    = My_join::join_portfolio();
        }

    	return view('admin/dashboard',$result);
    }
}
