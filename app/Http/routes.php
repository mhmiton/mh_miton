<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Web\Web_page@view_page');
Route::post('contact_action', 'Web\Web_page@contact_action');


// Back End Section
Route::get('login','Auth\AuthController@');
//Route::auth();

Route::get('dashboard', 'Admin\Admin_home@dashboard');
Route::get('admin', 'Admin\Admin_home@dashboard');
Route::get('pages/{page_content}/{db_data}/{order}','Admin\Admin_home@pages');


Route::get('inbox',function(){
	return view('admin.dashboard')->with('page_content','inbox');
});

Route::get('compose',function(){
	return view('admin.dashboard')->with('page_content','compose');
});

//About Crud Section
Route::post('about_store', 'Admin\About_crud@store');
Route::post('about_edit','Admin\About_crud@edit');
Route::get('about_clear/{id}','Admin\About_crud@delete');

//Skills Crud Section
Route::post('skills_store', 'Admin\Skills_crud@store');
Route::post('skills_edit','Admin\Skills_crud@edit');
Route::get('skills_clear/{id}','Admin\Skills_crud@delete');

//Service Crud Section
Route::post('service_store', 'Admin\Service_crud@store');
Route::post('service_edit','Admin\Service_crud@edit');
Route::get('service_clear/{id}','Admin\Service_crud@delete');

//Portfolio Category Crud Section
Route::post('port_category_store', 'Admin\Portfolio_category_crud@store');
Route::post('port_category_edit','Admin\Portfolio_category_crud@edit');
Route::get('port_category_clear/{id}','Admin\Portfolio_category_crud@delete');

//Portfolio Details Crud Section
Route::post('port_details_store', 'Admin\Portfolio_details_crud@store');
Route::post('port_details_edit','Admin\Portfolio_details_crud@edit');
Route::get('port_details_clear/{id}','Admin\Portfolio_details_crud@delete');

//Testimonial Manage Crud Section
Route::post('testimonial_store', 'Admin\Testimonial_crud@store');
Route::post('testimonial_edit','Admin\Testimonial_crud@edit');
Route::get('testimonial_clear/{id}','Admin\Testimonial_crud@delete');

//Contact Manage Crud Section
Route::post('contact_store', 'Admin\Contact_crud@store');
Route::post('contact_edit','Admin\Contact_crud@edit');
Route::get('contact_clear/{id}','Admin\Contact_crud@delete');

//Stat Manage Crud Section
Route::post('stat_store', 'Admin\Stat_crud@store');
Route::post('stat_edit','Admin\Stat_crud@edit');
Route::get('stat_clear/{id}','Admin\Stat_crud@delete');

//Slide Manage Crud Section
Route::post('slide_store', 'Admin\Slide_crud@store');
Route::post('slide_edit','Admin\Slide_crud@edit');
Route::get('slide_clear/{id}','Admin\Slide_crud@delete');

//Basic Manage Crud Section
Route::post('basic_store', 'Admin\Basic_crud@store');
Route::post('basic_edit','Admin\Basic_crud@edit');
Route::get('basic_clear/{id}','Admin\Basic_crud@delete');

//Social Manage Crud Section
Route::post('social_store', 'Admin\Social_crud@store');
Route::post('social_edit','Admin\Social_crud@edit');
Route::get('social_clear/{id}','Admin\Social_crud@delete');

//Menu Manage Crud Section
Route::post('menu_store', 'Admin\Menu_crud@store');
Route::post('menu_edit','Admin\Menu_crud@edit');
Route::get('menu_clear/{id}','Admin\Menu_crud@delete');

Route::auth();

Route::get('/home', 'HomeController@index');
