<!-- Content Header (Page header) -->
<section class="content-header">
  <button class="btn btn-flat btn-sm btn-success add_chart_btn" data-toggle="modal" data-target="#my_modal" aria-hidden="true" onclick="reset('#form');"><i class="fa fa-plus"></i> Add New Chart</button>

  <ol class="breadcrumb">
    <li><a href="/admin" tabindex="-1"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Add Social Media</li>
  </ol>
</section>

<!-- Form modal -->
<form name="form" id="form" action="/social_store" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
  <div class="modal fade" id="my_modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <i class="close" data-dismiss="modal" aria-label="Close" onclick="reset('#form');">
            <span aria-hidden="true">&times;</span></i>
          <h4 class="modal-title">Add Social Media</h4>
        </div>

        <div class="modal-body">

          <div class="form-group">
            <label class="form_heading">Icon</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <a class="pull-right" href="https://fontawesome.com/v4.7.0/icons/">See Icon List</a>
            <input class="form-control" type="text" name="icon" id="icon" placeholder="Font-awesome Icon (fa fa-icon)" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Url</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="url" name="url" id="url" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Status</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <select class="form-control" name="status" id="status" required style="width: 100%;">
              <option selected="selected" value="">Select Status</option>
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
            </select>
          </div>

        </div>

        <div class="modal-footer">
          <input class="hide_input" type="text" name="id" id="id">

          <span class="btn btn-flat btn-danger" data-dismiss="modal" onclick="reset('#form');">Close</span>
          <button type="submit" name="submit" id="submit" class="btn btn-flat btn-success">Save</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- End Form modal -->

<!-- Table content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive table_wrap">
            <table id="data_table" class="table table-striped table-hover">
              <thead class="table_head">
                <tr>
                  <td scope="col" align="left">SL</td>
                  <td scope="col" align="center">Icon</td>
                  <td scope="col" align="left">Url</td>
                  <td scope="col" align="center">Status</td>
                  <td scope="col" align="right">Action</td>
                </tr>
              </thead>

              <tbody class="table_data">
              @foreach($social_data as $key => $v)
                <tr>
                  <td scope="row" align="left"><span class="col">{{ $key+1 }}</span></td>
                  <td align="center"><span class="col {{ $v->icon }}"></span></td>
                  <td align="left"><a href="{{ $v->url }}" target="_blank" class="col">{{ $v->url }}</a></td>
                  <td align="center"><span class="col">{{ $v->status }}</span></td>

                  <td align="right">
                    <i class="fa fa-pencil edit_btn" data-id="{{ $v->so_id }}"></i>
                    <i class="fa fa-trash del_btn" data-href="social" data-id="{{ $v->so_id }}"></i>
                    <span class="col"></span>
                  </td>
                </tr>
              @endforeach
              </tbody>

            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- End Table content -->
<script type="text/javascript">
  $(document).ready(function(){

    $('.edit_btn').click(function(){
      var id = $(this).data('id');
      var token = $('input[name=_token]').val();
      $('table, .edit_btn').css({'cursor':'progress'});

      $.ajax({
        type:'POST',
        url:'/social_edit',
        data:{'_token':token,'id':id},
        dataType:'json',
        success:function(data)
        {
          $('#icon').val(data.icon);
          $('#url').val(data.url);
          $('#status').val(data.status);

          $('#id').val(data.so_id);
          $('#submit').text('Update');
          $('#my_modal').modal('show');
          $('table, .edit_btn').css({'cursor':'auto'});
        }
      });
    });

  });
</script>