<!-- Content Header (Page header) -->
<section class="content-header">
  <button class="btn btn-flat btn-sm btn-success add_chart_btn" data-toggle="modal" data-target="#my_modal" aria-hidden="true" onclick="reset('#form');" style="display:none;"><i class="fa fa-plus"></i> Add New Chart</button>

  <button class="btn btn-flat btn-sm btn-success basic add_chart_btn" data-id="1"><i class="fa fa-edit"></i> Edit Chart</button>

  <ol class="breadcrumb">
    <li><a href="/admin" tabindex="-1"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Basic Manage</li>
  </ol>
</section>

<!-- Form modal -->
<form name="form" id="form" action="/basic_store" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
  <div class="modal fade" id="my_modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <i class="close" data-dismiss="modal" aria-label="Close" onclick="reset('#form');">
            <span aria-hidden="true">&times;</span></i>
          <h4 class="modal-title">Basic Manage</h4>
        </div>

        <div class="modal-body">

          <div class="form-group">
            <label class="form_heading">Title</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="text" name="title" id="title" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Website Name</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="text" name="web_name" id="web_name" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Logo</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <span class="pull-right bg-danger file_name l_img" style="padding:0px 5px;"></span>
            <input class="form-control file" type="file" name="logo" id="logo" onchange="img_valid($(this));">
          </div>

          <div class="form-group">
            <label class="form_heading">Phone</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input type="number" min="0" class="form-control" name="phone" id="phone" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Email</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="email" name="email" id="email" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Copyright Name</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="text" name="copy_name" id="copy_name" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Copyright Link</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="text" name="copy_link" id="copy_link" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Copyright Year</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input type="number" min="0" max="{{ date('Y') }}" class="form-control" name="copy_year" id="copy_year" required placeholder="yyyy">
          </div>

        </div>

        <div class="modal-footer">
          <input class="hide_input" type="text" name="id" id="id">

          <span class="btn btn-flat btn-danger" data-dismiss="modal" onclick="reset('#form');">Close</span>
          <button type="submit" name="submit" id="submit" class="btn btn-flat btn-success">Save</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- End Form modal -->

<!-- Table content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive table_wrap">
            <table id="data_table" class="table table-striped table-hover">
              <thead class="table_head">
                <tr>
                  <td scope="col" align="left">SL</td>
                  <td scope="col" align="center">Title</td>
                  <td scope="col" align="left">Website Name</td>
                  <td scope="col" align="center">Logo</td>
                  <td scope="col" align="center">Phone</td>
                  <td scope="col" align="center">Email</td>
                  <td scope="col" align="right">Action</td>
                </tr>
              </thead>

              <tbody class="table_data">
              @foreach($basic_data as $key => $v)
                <tr>
                  <td scope="row" align="left"><span class="col">{{ $key+1 }}</span></td>
                  <td align="center"><span class="col">{{ $v->title }}</span></td>
                  <td align="left"><span class="col">{{ $v->web_name }}</span></td>

                  <td align="center">
                    <span class="col">
                      {!! Html::image('upload/logo/'.$v->logo,'Image', array('width'=>70,'height'=>40)) !!}
                    </span>
                  </td>

                  <td align="center"><span class="col">{{ $v->phone }}</span></td>
                  <td align="center"><span class="col">{{ $v->email }}</span></td>

                  <td align="right">
                    <i class="fa fa-pencil edit_btn" data-id="{{ $v->ba_id }}"></i>
                    <!-- <i class="fa fa-trash del_btn" data-href="basic" data-id="{{ $v->ba_id }}"></i> -->
                    <span class="col"></span>
                  </td>
                </tr>
              @endforeach
              </tbody>

            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- End Table content -->
<script type="text/javascript">
  $(document).ready(function(){

    $('.edit_btn, .basic').click(function(){
      var id = $(this).data('id');
      var token = $('input[name=_token]').val();
      $('table, .edit_btn').css({'cursor':'progress'});

      $.ajax({
        type:'POST',
        url:'/basic_edit',
        data:{'_token':token,'id':id},
        dataType:'json',
        success:function(data)
        {
          $('#title').val(data.title);
          $('#web_name').val(data.web_name);
          $('.l_img').text(data.logo);
          $('#phone').val(data.phone);
          $('#email').val(data.email);
          $('#copy_name').val(data.copy_name);
          $('#copy_link').val(data.copy_link);
          $('#copy_year').val(data.copy_year);

          $('#id').val(data.ba_id);
          $('#submit').text('Update');
          $('#my_modal').modal('show');
          $('table, .edit_btn').css({'cursor':'auto'});
        }
      });
    });

  });
</script>