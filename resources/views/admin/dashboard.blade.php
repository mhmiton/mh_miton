<!DOCTYPE html>
<html>
<head>
  @include('admin.common.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Header -->
  @include('admin.common.header')
  <!-- left-sidebar -->
  @include('admin.common.left_sidebar')
  

  @php
    $alert = session()->get('alert');
    $type = session()->get('type');
    session()->forget(['alert'=>'alert','type'=>'type']);
  @endphp

  @if($alert)
    <script type="text/javascript">
      //swal({title:'{{$alert}}', icon:'{{$type}}', button:true});
      $.notify('{{$alert}}','{{$type}}');
    </script>
  @endif

  <!-- Content Wrapper. Contains page content -->
  <div class="full_window" style="min-height: 900px !important; background-color: #ecf0f5;">
    <div class="content-wrapper">
      @if($page_content == null)
        @include('admin.dash_content')
      @else
        @include('admin.'.$page_content)
      @endif
    </div>
  </div>
  <!-- /.content-wrapper -->


  <!-- Footer Section -->
  @include('admin.common.footer')

  <!-- Control/Right Sidebar -->
  @include('admin.common.right_sidebar')


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

@include('admin.common.js_link')
<!-- ./wrapper -->

</body>
</html>
