@php
  $basic_data = session()->get('basic_data');
  $owner = session()->get('owner');
@endphp

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      Design & Develop By - <b>{{ $owner->name }}</b>
    </div>
    <strong>
    	Copyright &copy; {{ $basic_data->copy_year }}
    	<a target="_blank" href="{{ $basic_data->copy_link }}">{{ $basic_data->copy_name }}</a>.
	</strong> 
	All rights reserved.
</footer>