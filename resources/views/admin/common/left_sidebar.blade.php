@php
  $basic_data = session()->get('basic_data');
  $owner = session()->get('owner');     
@endphp

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
        {{ Html::image('/upload/logo/'.$basic_data->logo, 'User Image', array('class' => 'img-circle') ) }}
        </div>
        <div class="pull-left info">
          <p>{{ $owner->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li><a href="/admin"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-cog"></i> <span>Setup</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/pages/basic_manage/basic_data/ba"><i class="fa fa-circle-o"></i>Basic Manage</a></li>
            <li><a href="/pages/menu_manage/menu_data/me"><i class="fa fa-circle-o"></i>Menu Manage</a></li>
            <li><a href="/pages/social/social_data/so"><i class="fa fa-circle-o"></i>Add Social Media</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-podcast"></i> <span>Portfolio Manage</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/pages/portfolio_category/port_category/port"><i class="fa fa-circle-o"></i>Portfolio Category</a></li>
            <li><a href="/pages/portfolio_details/details/de"><i class="fa fa-circle-o"></i>Portfolio Details</a></li>
          </ul>
        </li>

        <!-- <li><a href="/inbox"><i class="fa fa-envelope"></i> <span>Inbox</span></a></li> -->

        <li><a href="/pages/slide_manage/slide_data/sl"><i class="fa fa-camera-retro"></i> <span>Slide Manage</span></a></li>

        <li><a href="/pages/about_manage/about_data/ab"><i class="fa fa-user-circle-o"></i> <span>About Manage</span></a></li>

        <li><a href="/pages/skills_manage/skills_data/sk"><i class="fa fa-superpowers"></i> <span>Skills Manage</span></a></li>

        <li><a href="/pages/service_manage/service_data/se"><i class="fa fa-ravelry"></i> <span>Service Manage</span></a></li>

        <li><a href="/pages/testimonial_manage/testimonial_data/te"><i class="fa fa-thumbs-o-up"></i> <span>Testimonial Manage</span></a></li>
        <li><a href="/pages/stat_manage/stat_data/st"><i class="fa fa-bar-chart"></i> <span>Stat Manage</span></a></li>
        <li><a href="/pages/contact_manage/contact_data/co"><i class="fa fa-volume-control-phone"></i> <span>Contact Manage</span></a></li>
        <li><a href="/logout"><i class="fa fa-sign-out"></i> <span>Log Out</span></a></li>

        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Examples</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Login</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-circle-o"></i> 500 Error</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Blank Page</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Pace Page</a></li>
          </ul>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>