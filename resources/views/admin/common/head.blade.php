@php
  $basic_data = session()->get('basic_data');
  $owner = session()->get('owner');
@endphp

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Mehediul Hassan Miton - Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
{{ Html::script('assets/back_end/bower_components/jquery/dist/jquery.min.js') }}

{{ Html::favicon('upload/logo/'.$basic_data->logo) }}
{{ Html::style('assets/back_end/bower_components/bootstrap/dist/css/bootstrap.min.css') }}
{{ Html::style('assets/back_end/bower_components/font-awesome/css/font-awesome.min.css') }}
{{ Html::style('assets/back_end/bower_components/Ionicons/css/ionicons.min.css') }}
{{ Html::style('assets/back_end/dist/css/AdminLTE.min.css') }}
{{ Html::style('assets/back_end/dist/css/skins/_all-skins.min.css') }}
{{ Html::style('assets/back_end/bower_components/morris.js/morris.css') }}
{{ Html::style('assets/back_end/bower_components/jvectormap/jquery-jvectormap.css') }}
{{ Html::style('assets/back_end/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
{{ Html::style('assets/back_end/bower_components/select2/dist/css/select2.css') }}
{{ Html::style('assets/back_end/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}
{{ Html::style('assets/back_end/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}

{{ Html::style('assets/back_end/richText_editor/css/richtext.min.css') }}
{{ Html::style('assets/back_end/Tag-Input/tagsinput.css') }}

<!-- Plagins -->
{{ Html::style('assets/back_end/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}
{{ Html::style('assets/back_end/plugins/iCheck/flat/blue.css') }}


<!-- MY ASSETS -->
{{ Html::style('assets/back_end/my_assets/css/custom.css') }}
{{ Html::script('assets/back_end/my_assets/js/sweetalert.min.js') }}
{{ Html::script('assets/back_end/my_assets/js/notify.min.js') }}
{{ Html::script('assets/back_end/my_assets/js/custom.js') }}

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">