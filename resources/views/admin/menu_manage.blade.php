<!-- Content Header (Page header) -->
<section class="content-header">
  <button class="btn btn-flat btn-sm btn-success add_chart_btn" data-toggle="modal" data-target="#my_modal" aria-hidden="true" onclick="reset_file(),reset('#form');"><i class="fa fa-plus"></i> Add New Chart</button>

  <ol class="breadcrumb">
    <li><a href="/admin" tabindex="-1"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Menu Manage</li>
  </ol>
</section>

<!-- Form modal -->
<form name="form" id="form" action="/menu_store" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
  <div class="modal fade" id="my_modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <i class="close" data-dismiss="modal" aria-label="Close" onclick="reset('#form');">
            <span aria-hidden="true">&times;</span></i>
          <h4 class="modal-title">Menu Manage</h4>
        </div>

        <div class="modal-body">

          <div class="form-group">
            <label class="form_heading">Menu Name</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="text" name="menu_name" id="menu_name" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Menu Heading</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="text" name="menu_heading" id="menu_heading" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Image</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <span class="pull-right bg-danger file_name m_img" style="padding:0px 5px;"></span>
            <input class="form-control file" type="file" name="menu_img" id="menu_img" onchange="img_valid($(this));" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Image Align</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <select class="form-control" name="img_align" id="img_align" required style="width: 100%;">
              <option selected="selected" value="">Select Image Align</option>
              <option value="left">Left</option>
              <option value="right">Right</option>
            </select>
          </div>

          <div class="form-group">
            <label class="form_heading">Status</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <select class="form-control" name="status" id="status" required style="width: 100%;">
              <option selected="selected" value="">Select Status</option>
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
            </select>
          </div>

          <div class="form-group">
            <label class="form_heading">Description</label>
            <textarea class="form-control" name="menu_desc" id="editor"></textarea>
          </div>

        </div>
        <div class="modal-footer">
          <input class="hide_input" type="text" name="id" id="id">

          <span class="btn btn-flat btn-danger" data-dismiss="modal" onclick="reset('#form');">Close</span>
          <button type="submit" name="submit" id="submit" class="btn btn-flat btn-success">Save</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- End Form modal -->

<!-- Table content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive table_wrap">
            <table id="data_table" class="table table-striped table-hover">
              <thead class="table_head">
                <tr>
                  <td scope="col" align="left">SL</td>
                  <td scope="col" align="left">Menu Name</td>
                  <td scope="col" align="left">Menu Heading</td>
                  <td scope="col" align="center">Image</td>
                  <td scope="col" align="left">Description</td>
                  <td scope="col" align="center">Status</td>
                  <td scope="col" align="right">Action</td>
                </tr>
              </thead>

              <tbody class="table_data">
              @foreach($menu_data as $key => $v)
                <tr>
                  <td scope="row" align="left"><span class="col">{{ $key+1 }}</span></td>
                  <td align="left"><span class="col">{{ $v->menu_name }}</span></td>
                  <td align="left"><span class="col">{{ $v->menu_heading }}</span></td>

                  <td align="center">
                    <span class="col">
                      {!! Html::image('upload/menu/'.$v->menu_img,'Image', array('width'=>70,'height'=>40)) !!}
                    </span>
                  </td>

                  <td align="left">
                    <span class="col">
                      {!! strip_tags(substr($v->menu_desc,0,30)) !!}...
                    </span>
                  </td>

                  <td align="center"><span class="col">{{ $v->status }}</span></td>

                  <td align="right">
                    <i class="fa fa-pencil edit_btn" data-id="{{ $v->me_id }}"></i>
                    <i class="fa fa-trash del_btn" data-href="menu" data-id="{{ $v->me_id }}"></i>
                    <span class="col"></span>
                  </td>
                </tr>
              @endforeach
              </tbody>

            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- End Table content -->
<script type="text/javascript">
  $(document).ready(function(){

    $('.edit_btn').click(function(){
      var id = $(this).data('id');
      var token = $('input[name=_token]').val();
      $('table, .edit_btn').css({'cursor':'progress'});
      $('.file').removeAttr('required',true);

      $.ajax({
        type:'POST',
        url:'/menu_edit',
        data:{'_token':token,'id':id},
        dataType:'json',
        success:function(data)
        {
          $('#menu_name').val(data.menu_name);
          $('#menu_heading').val(data.menu_heading);
          $('.m_img').text(data.menu_img);
          $('#img_align').val(data.img_align);
          $('#status').val(data.status);
          CKEDITOR.instances.editor.setData(data.menu_desc);

          $('#id').val(data.me_id);
          $('#submit').text('Update');
          $('#my_modal').modal('show');
          $('table, .edit_btn').css({'cursor':'auto'});
        }
      });
    });

  });
</script>