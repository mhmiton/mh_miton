<!-- Content Header (Page header) -->
<section class="content-header">
  <button class="btn btn-flat btn-sm btn-success add_chart_btn" data-toggle="modal" data-target="#my_modal" aria-hidden="true" onclick="reset_file(),reset('#form');"><i class="fa fa-plus"></i> Add New Chart</button>

  <ol class="breadcrumb">
    <li><a href="/admin" tabindex="-1"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Portfolio Details</li>
  </ol>
</section>

<!-- Form modal -->
<form name="form" id="form" action="/port_details_store" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
  <div class="modal fade" id="my_modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <i class="close" data-dismiss="modal" aria-label="Close" onclick="reset('#form');">
            <span aria-hidden="true">&times;</span></i>
          <h4 class="modal-title">Portfolio Details</h4>
        </div>

        <div class="modal-body">

          <div class="form-group">
            <label class="form_heading">Select Category</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <select class="form-control" name="port_cat_id" id="port_cat_id" required style="width: 100%;">
              <option selected="selected" value="">Select Category</option>
              @foreach($port_category as $key => $v)
                <option value="{{ $v->port_id }}">{{ $v->port_category }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label class="form_heading">Project Name</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="text" name="project_name" id="project_name" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Project Url</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="text" name="project_url" id="project_url" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Client</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <input class="form-control" type="text" name="client" id="client" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Date</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" class="form-control date" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask name="date" id="date" required>
            </div>
          </div>

          <div class="form-group">
            <label class="form_heading">Image</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <span class="pull-right bg-danger file_name p_img" style="padding:0px 5px;"></span>
            <input class="form-control file" type="file" name="project_img" id="project_img" onchange="img_valid($(this));" required>
          </div>

          <div class="form-group">
            <label class="form_heading">Status</label><i class="fa fa-star fill" aria-hidden="true"></i>
            <select class="form-control" name="status" id="status" required style="width: 100%;">
              <option selected="selected" value="">Select Status</option>
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
            </select>
          </div>

          <div class="form-group">
            <label class="form_heading">Description</label>
            <textarea class="form-control" name="project_desc" id="editor"></textarea>
          </div>

        </div>
        <div class="modal-footer">
          <input class="hide_input" type="text" name="id" id="id">

          <span class="btn btn-flat btn-danger" data-dismiss="modal" onclick="reset('#form');">Close</span>
          <button type="submit" name="submit" id="submit" class="btn btn-flat btn-success">Save</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- End Form modal -->

<!-- Table content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive table_wrap">
            <table id="data_table" class="table table-striped table-hover">
              <thead class="table_head">
                <tr>
                  <td scope="col" align="left">SL</td>
                  <td scope="col" align="left">Category</td>
                  <td scope="col" align="left">Project Name</td>
                  <td scope="col" align="left">Project Url</td>
                  <td scope="col" align="left">Client</td>
                  <td scope="col" align="center">Date</td>
                  <td scope="col" align="center">Image</td>
                  <td scope="col" align="center">Status</td>
                  <td scope="col" align="right">Action</td>
                </tr>
              </thead>

              <tbody class="table_data">
              @foreach($port_details as $key => $v)
                <tr>
                  <td scope="row" align="left"><span class="col">{{ $key+1 }}</span></td>
                  <td align="left"><span class="col">{{ $v->port_category }}</span></td>
                  <td align="left"><span class="col">{{ $v->project_name }}</span></td>
                  <td align="left"><span class="col">{{ $v->project_url }}</span></td>
                  <td align="left"><span class="col">{{ $v->client }}</span></td>
                  <td align="center"><span class="col">{{ $v->date_str }}</span></td>

                  <td align="center">
                    <span class="col">
                      {!! Html::image('upload/portfolio/'.$v->project_img,'Image', array('width'=>70,'height'=>40)) !!}
                    </span>
                  </td>

                  <td align="center"><span class="col">{{ $v->status }}</span></td>

                  <td align="right">
                    <i class="fa fa-pencil edit_btn" data-id="{{ $v->de_id }}"></i>
                    <i class="fa fa-trash del_btn" data-href="port_details" data-id="{{ $v->de_id }}"></i>
                    <span class="col"></span>
                  </td>
                </tr>
              @endforeach
              </tbody>

            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- End Table content -->
<script type="text/javascript">
  $(document).ready(function(){

    $('.edit_btn').click(function(){
      var id = $(this).data('id');
      var token = $('input[name=_token]').val();
      $('table, .edit_btn').css({'cursor':'progress'});
      $('.file').removeAttr('required',true);

      $.ajax({
        type:'POST',
        url:'/port_details_edit',
        data:{'_token':token,'id':id},
        dataType:'json',
        success:function(data)
        {
          var date = data.date.split('-');
          $('#port_cat_id').val(data.port_cat_id);
          $('#project_name').val(data.project_name);
          $('#project_url').val(data.project_url);
          $('#client').val(data.client);
          $('#date').val(date[2]+'-'+date[1]+'-'+date[0]);
          $('.p_img').text(data.project_img);
          $('#status').val(data.status);
          CKEDITOR.instances.editor.setData(data.project_desc);

          $('#id').val(data.de_id);
          $('#submit').text('Update');
          $('#my_modal').modal('show');
          $('table, .edit_btn').css({'cursor':'auto'});
        }
      });
    });

  });
</script>