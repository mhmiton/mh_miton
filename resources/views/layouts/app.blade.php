<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mehediul Hassan Miton - Dashboard</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {{ Html::style('assets/back_end/bower_components/font-awesome/css/font-awesome.min.css') }}
    {{ Html::style('assets/back_end/bower_components/bootstrap/dist/css/bootstrap.min.css') }}
    {{ Html::style('assets/back_end/my_assets/css/admin_index.css') }}
    <link href="https://fonts.googleapis.com/css?family=Arvo|Dosis|Exo+2" rel="stylesheet">
</head>
<body>


<!--LOG IN & REGISTER SECTION-->
    
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h1>Mehediul Hassan Miton</h1>
                        <small>An Web Developer</small>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-lg-6 div_center">                  
                        <div class="log_in_wrap">
                            @yield('content')
                        </div>
                    </div>
                </div>  

            </div>
        </div>
    </div>

<!--END LOG IN & REGISTER SECTION-->

</body>
</html>