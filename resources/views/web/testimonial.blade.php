<div id="testimonial" class="testimonial-area section-padding">
    <div class="container">
       <div class="row">
           <div class="col-md-12 text-center">
              <div class="section-title">
               <h2>what people say.</h2>
               </div>
           </div>
       </div> <!--/.row-->
       
      <div class="row">
        <div class="col-md-12">
            <div class="testimonial-list">
                @foreach($test_data as $v)
                    <div class="single-testimonial"> <!-- Single testimonial  -->
                        <div class="t-author">
                            <div class="t-image">
                                {{ Html::image('/upload/testimonial/'.$v->image, 'Image') }}
                            </div>
                            <div class="t-name">
                                <h4>{{ $v->name }}</h4>
                                {{ $v->designation }}
                            </div>
                        </div>
                        <div class="t-content">
                            <p>{!! $v->description !!}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div> <!-- / row -->
    </div> <!--/.container-->
  </div>
</div>