<div id="contact" class="contact-info-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="section-title">
               <h2>contact me.</h2>
                  <p>Feel Free to Contact</p>
               </div>
            </div>
        </div> <!--/.row-->
        
        <div class="row">
            <div class="col-md-7">
                  <div class="contact-form">
                    <form id="contact-form" method="post" action="/contact_action">
                     <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                {{ csrf_field() }}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <i class="fa fa-user-o"></i>
                                        <input id="form_name" type="text" name="name" class="form-control" placeholder="Name*" required="required" data-error="Name is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <i class="fa fa-envelope-o"></i>
                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="Email*" required="required" data-error="Valid email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <i class="fa fa-question-circle-o"></i>
                                        <input id="form_subject" type="text" name="subject" class="form-control" placeholder="Subject*" required="required" data-error="Subject is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <i class="fa fa-comment-o"></i>
                                        <textarea id="form_message" name="message" class="form-control" placeholder="Message*" rows="7" required="required" data-error="Please,leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-effect btn-sent" value="Send message">
                                </div>
                            </div>
                        </div>
                    </form> <!-- End Contact From -->
                  </div>
             </div>
            
            <div class="col-md-5">
                @foreach($contact_data as $v)
                    <div class="single-info"> <!-- Single Info -->
                        <div class="info-icon">
                            <i class="fa {{ $v->icon }}"></i>
                        </div>
                        <div class="info-content">
                            <h5>{{ $v->heading }}:</h5>
                            <p>{{ $v->info }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div> <!--/.row-->
    </div> <!--/.container-->
</div>