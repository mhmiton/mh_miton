@php
    use App\My_model;
@endphp
<div id="work" class="work-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="section-title">
                   <h2>recent work.</h2>
                   <p>Here's some of my recent work</p>
               </div>
            </div>
        </div> <!--/.row-->
        <div class="row">
            <ul class="work-list text-center">
                <li class="filter" data-filter="all">all</li>
                @foreach($port_category as $v)
                    <li class="filter" data-filter=".port{{ $v->port_id }}">{{ $v->port_category }}</li>
                @endforeach
            </ul>
        </div> <!--/.row-->
        
        <div class="work-inner">
            <div class="row">
                @foreach($port_category as $C)
                    @php
                        $where = ['status'=>'Active','port_cat_id'=>$C->port_id];
                        $port_details = My_model::get_all_row('portfolio_details',$where,'','');
                    @endphp

                    @foreach($port_details as $D)
                        <div class="col-md-4 col-sm-6 col-xs-12 mix port{{ $D->port_cat_id }}"> <!-- Single Work -->
                            <div class="single-work">
                                {{ Html::image('/upload/portfolio/'.$D->project_img, 'Image') }}
                                <div class="item-hover">
                                    <div class="work-table">
                                        <div class="work-tablecell">
                                            <div class="hover-content">
                                                <h4>{{ $D->project_name }}</h4>
                                                <a href="#" class="work-link" data-toggle="modal" data-target="#projectModal{{ $D->de_id }}"><i class="fa fa-link"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Work Modal  -->
                        <div  tabindex="0" class="modal fade" id="projectModal{{ $D->de_id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Project Overview</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{ Html::image('/upload/portfolio/'.$D->project_img, 'Image') }}
                                        <h3>{{ $D->project_name }}</h3>
                                        <p>{!! $D->project_desc !!}</p>
                                         <ul class="list-unstyled project-list" >
                                            <li><label>Client : </label> {{ $D->client }}</li>
                                            <li><label>Category :</label> {{ $C->port_category }}</li>
                                            <li><label>Date : </label> {{ $D->date_str }}</li>
                                            <li><label>Project Url : </label> <a href="{{ $D->project_url }}">{{ $D->project_url }}</a></li>
                                        </ul>
                                  </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>
                        <!-- End Work Modal  -->
                       
                    @endforeach
                @endforeach					
            </div> <!--/.row-->
        </div>
    </div><!--/.container-->
</div>