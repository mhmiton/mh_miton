<div class="stat-area section-padding">
    <div class="container">
        <div class="row">
            @foreach($stat_data as $v)
                <div class="col-md-3 col-sm-6"> <!-- Single stat  -->
                    <div class="single-stat">
                        <div class="inner">
                            <div class="stat-icon-box">
                                <i class="{{ $v->icon }}"></i>
                            </div>
                           <div class="stat-content">
                                <h2 class="counter">{{ $v->count_number }}</h2>
                                <h3>{{ $v->title }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div> <!--/.row -->
    </div> <!--/.container -->
</div>