<!DOCTYPE html>
<html lang="en">
    <head>
        @include('web.common.head')
    </head>

    <body>
        
        <!--======= PRELOADER =======-->
<!--         <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div> -->
        
        <!--==== DEMO COLOR PANEL ===-->
        <!-- <div class="demo_panel_box">
            <div class="color_panel_box">
                <div class="spiner_button slide_in_out"><i class="fa fa-cog fa-spin"></i></div>
                <span class="color_1"></span>
                <span class="color_2"></span>
                <span class="color_3"></span>
                <span class="color_4"></span>
                <span class="color_5"></span>
                <span class="color_6"></span>
            </div>
        </div> -->
        <!--==== DEMO COLOR PANEL ===-->

        @include('web.common.header')
        @include('web.slider')
        @include('web.about')
        @include('web.services')
        @include('web.work')
        @include('web.stat')
        @include('web.testimonial')
        @include('web.cta')
        @include('web.menu_page')
        @include('web.contact')
        @include('web.common.footer')

    </body>
</html>
