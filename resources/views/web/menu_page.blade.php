@foreach ($menu_data as $key => $v)
    <div id="{{ $v->me_id }}" class="about-area section-padding" style="background: @if(($key+1)%2 == 0) #f7f7f7; @else #fff; @endif">
        <div class="container">
            <div class="row">
                <div class="section-title">
                   <h2>{{ $v->menu_heading }}.</h2>
                </div>

                <div class="col-md-5 pull-{{ $v->img_align }}">
                   <div class="author-image" style="padding-top:10px;">
                        {{ Html::image('upload/menu/'.$v->menu_img,'Author Image') }}
                    </div>
                </div>

                <div class="col-md-7">
                    <h4 class="about-heading" style="padding-top:10px;">
                        <!-- I'm 24 years old creative <span>web</span> & <span>Graphic</span> Designer -->
                        {{ $v->menu_heading }}
                    </h4>

                   <p>{!! $v->menu_desc !!}</p>
                </div>
            </div> <!--/.row-->
        </div>
    </div>
@endforeach
