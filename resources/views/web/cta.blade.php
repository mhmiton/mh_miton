<div class="cta-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="wow fadeInLeft" data-wow-delay="0.4s">Do you have any project?</p>
                <h2 class="wow fadeInRight" data-wow-delay="0.8s">Let's work together indeed!</h2>
                <a href="#contact" class="smoth-scroll wow fadeInDown" data-wow-delay="1.2s">get quotes</a>
            </div>
        </div>
    </div>
</div>