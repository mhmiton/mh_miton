<div id="about" class="about-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
               <div class="author-image">
                    {{ Html::image('upload/about/'.$about_data->about_img,'Author Image') }}
                </div>
            </div>

            <div class="col-md-7">
                <div class="tab" role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist"> <!-- Nav tabs -->
                        <li role="presentation" class="active"><a href="#Section1" role="tab" data-toggle="tab">about</a></li>
                        <li role="presentation"><a href="#Section2" role="tab" data-toggle="tab">Skills</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="Section1"> <!-- Single Tab -->
                            <h4 class="about-heading">{{ $about_data->about_heading }}</h4>
                            <!-- I'm 24 years old creative <span>web</span> & <span>Graphic</span> Designer-->
                           <p>
                               {!! $about_data->about_desc !!}
                           </p>
                            <ul class="social-links"> <!--=== social-links ===-->
                                @foreach($social_data as $v)
                                    <li><a target="_blank" href="{{ $v->url }}"><i class="{{ $v->icon }}"></i></a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="Section2"> <!-- Single Tab -->
                            @foreach($skills_data as $v)
                                <div class="single-skill"> <!-- Single Skill -->
                                    <div class="skill-info">
                                        <h4>{{ $v->skills_name }}</h4>
                                    </div>
                                    <div class="progress">
                                      <div class="progress-bar" role="progressbar" aria-valuenow="{{ $v->skills_count }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $v->skills_count }}%;"><span>{{ $v->skills_count }}%</span></div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--/.row-->
    </div> <!--/.container-->
</div>