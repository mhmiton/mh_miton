<footer class="footer">
    <div class="container">
        <div class="row wow zoomIn" data-wow-delay="0.4s">
            <div class="col-md-12 text-center">
                <p>&copy;{{ $basic_data->copy_year }} <strong>{{ $basic_data->copy_name }}</strong>. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>

{{ Html::script('assets/front_end/js/plugins.js') }}
{{ Html::script('assets/front_end/js/typed.js') }}
{{ Html::script('assets/front_end/js/jquery.stellar.min.js') }}
{{ Html::script('assets/front_end/js/jquery.counterup.min.js') }}
{{ Html::script('assets/front_end/js/jquery.waypoints.min.js') }}
{{ Html::script('assets/front_end/js/wow.min.js') }}
{{ Html::script('assets/front_end/js/validator.min.js') }}
{{ Html::script('assets/front_end/js/jquery.mixitup.js') }}
{{ Html::script('assets/front_end/js/contact.js') }}
{{ Html::script('assets/front_end/js/main.js') }}