<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Mehediul Hassan Miton is a Personal Website">
<meta name="keywords" content="mhbthemes, resume, cv, portfolio, personal, developer, designer,personal resume, onepage, clean, modern">
<meta name="author" content="Mehediul Hassan Miton">

<title>{{ $basic_data->title }}</title>

{{ Html::favicon('upload/logo/'.$basic_data->logo) }}
{{ Html::style('assets/front_end/css/bootstrap.min.css') }}
{{ Html::style('assets/front_end/css/font-awesome.min.css') }}
{{ Html::style('assets/front_end/css/magnific-popup.css') }}
{{ Html::style('assets/front_end/css/owl.carousel.css') }}
{{ Html::style('assets/front_end/css/animate.min.css') }}
{{ Html::style('assets/front_end/css/style.css') }}
{{ Html::style('assets/front_end/css/responsive.css') }}
{{ Html::style('assets/front_end/css/theme-color.css') }}
{{ Html::script('assets/front_end/js/jquery-2.1.4.min.js') }}


<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
