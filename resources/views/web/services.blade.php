<div id="services" class="services-area section-padding">
    <div class="container">
       <div class="row">
           <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="section-title">
               <h2>what i do.</h2>
               </div>
           </div>
       </div> <!--/.row-->
       
        <div class="row">
            @foreach($service_data as $v)
                <div class="col-md-4 col-sm-6"> <!-- Single Service -->
                    <div class="single-service text-center wow fadeInUp" data-wow-delay="0.4s" style="height:300px;">
                       <div class="service-img" style="background-image:url(/upload/service/{{ $v->service_img }})"></div>
                        <h3>{{ $v->service_name }}</h3>
                        <p>{!! $v->service_desc  !!}</p>
                        <div class="service-overlay text-left">
                            <ul class="clearfix">
                                @php
                                    $SF = explode(',',$v->service_features);
                                @endphp
                                @for($i=0; $i < count($SF); $i++)
                                    <li><i class="fa fa-long-arrow-right"></i>{{ $SF[$i] }}</li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div> <!--/.row-->
    </div> <!--/.container-->
</div>