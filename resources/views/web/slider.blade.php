<div id="home" class="welcome-area" style="background-image: url(/upload/slider/{{$slide_data->slide_img}}); background-attachment: fixed;">
   <div class="welcome-table">
       <div class="welcome-cell">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="welcome-text">
                          <h1 class="theme-color">{{ $slide_data->heading }}</h1>
                           <div class="element"></div>
                            
                        </div>
                        <div class="home-arrow">
                            <a href="#about" class="smoth-scroll"><i class="fa fa-angle-double-down"></i></a>
                        </div>
                    </div>
               </div> <!--/.row-->
            </div> <!--/.container-->
        </div>
    </div>
</div>
@php
  $sub_heading = explode(',',$slide_data->sub_heading);
@endphp
<script type="text/javascript">
(function ($) {
   /*======== Intro typer ===========*/
    var element = $(".element");
    $(function() {
        element.typed({
            strings: {!! json_encode($sub_heading) !!},
            typeSpeed: 100,
            loop: true,
        });
    });
}(jQuery));
</script>