-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 03, 2018 at 08:22 PM
-- Server version: 5.5.59-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lv_mh_miton`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_manage`
--

CREATE TABLE IF NOT EXISTS `about_manage` (
  `ab_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `about_heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about_img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about_desc` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ab_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `about_manage`
--

INSERT INTO `about_manage` (`ab_id`, `about_heading`, `about_img`, `about_desc`, `status`, `created_at`, `updated_at`) VALUES
(1, 'I''M 24 YEARS OLD CREATIVE WEB & GRAPHIC DESIGNER', '1522248815.jpg', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 'Active', '2018-03-28 14:53:34', '2018-03-28 14:53:34'),
(2, 'I''M 24 YEARS OLD CREATIVE WEB & GRAPHIC DESIGNER', '1522248828.jpg', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 'Active', '2018-03-28 14:53:47', '2018-03-28 14:55:44'),
(3, 'I''M 24 YEARS OLD CREATIVE WEB & GRAPHIC DESIGNER', '1522248923.jpg', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 'Active', '2018-03-28 14:53:59', '2018-03-30 16:07:53'),
(4, 'I''M 24 YEARS OLD CREATIVE WEB & GRAPHIC DESIGNER', '1522774641.jpg', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 'Active', '2018-03-28 14:57:50', '2018-04-03 16:57:20');

-- --------------------------------------------------------

--
-- Table structure for table `basic_manage`
--

CREATE TABLE IF NOT EXISTS `basic_manage` (
  `ba_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `copy_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `copy_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `copy_year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ba_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `basic_manage`
--

INSERT INTO `basic_manage` (`ba_id`, `title`, `web_name`, `logo`, `phone`, `email`, `copy_name`, `copy_link`, `copy_year`) VALUES
(1, 'Mehediul Hasssan Miton - Personal Website', 'MHmiton', '1522786102.png', '01878988124', 'mdmiton321@gmail.com', 'MHmiton', 'https://mhmiton.tk', '2018');

-- --------------------------------------------------------

--
-- Table structure for table `contact_manage`
--

CREATE TABLE IF NOT EXISTS `contact_manage` (
  `co_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`co_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contact_manage`
--

INSERT INTO `contact_manage` (`co_id`, `icon`, `heading`, `info`, `status`) VALUES
(1, 'fa fa-rocket', 'My Location', 'Rampura, Dhaka - 1219', 'Active'),
(2, 'fa fa-phone', 'Phone Number', '01878988124', 'Active'),
(3, 'fa fa-envelope', 'Email Address', 'mdmiton321@gmail.com', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `menu_manage`
--

CREATE TABLE IF NOT EXISTS `menu_manage` (
  `me_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_align` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`me_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `menu_manage`
--

INSERT INTO `menu_manage` (`me_id`, `menu_name`, `menu_heading`, `menu_img`, `img_align`, `menu_desc`, `status`) VALUES
(2, 'my education', 'my education', '1522521800.jpg', 'right', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It', 'Inactive'),
(3, 'my family', 'my family', '1522521811.jpg', 'left', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It', 'Inactive'),
(4, 'my hobby', 'my hobby', '1522530445.jpg', 'right', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2018_03_26_063520_create_about_table', 1),
('2018_03_28_221023_create_skills_table', 2),
('2018_03_29_011037_create_service_table', 3),
('2018_03_29_234149_create_portfolio_category_table', 4),
('2018_03_30_005059_create_portfolio_details_table', 5),
('2018_03_30_191117_create_testimonial_manage', 6),
('2018_03_30_204128_create_contact_manage_tabel', 7),
('2018_03_30_204128_create_contact_manage_table', 8),
('2018_03_30_213255_create_stat_manage_table', 9),
('2018_03_30_232306_create_slide_manage_table', 10),
('2018_03_31_003752_create_basic_manage_table', 11),
('2018_03_31_234135_create_social_table', 12),
('2018_04_01_000521_create_menu_manage_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_category`
--

CREATE TABLE IF NOT EXISTS `portfolio_category` (
  `port_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `port_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`port_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `portfolio_category`
--

INSERT INTO `portfolio_category` (`port_id`, `port_category`, `status`) VALUES
(3, 'ILLUSTRATION', 'Active'),
(4, 'BRANDING', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_details`
--

CREATE TABLE IF NOT EXISTS `portfolio_details` (
  `de_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `port_cat_id` int(11) NOT NULL,
  `project_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `project_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `date_str` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `project_img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `project_desc` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`de_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `portfolio_details`
--

INSERT INTO `portfolio_details` (`de_id`, `port_cat_id`, `project_name`, `project_url`, `client`, `date`, `date_str`, `project_img`, `project_desc`, `status`) VALUES
(8, 3, 'Project Name Here', ' www.example.net', 'John Doe', '2018-02-27', '27 February 2018', '1522781193.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer porttitor massa sed velit egestas vulputate. Morbi turpis tellus, porta in cursus at, finibus vitae dui. Nam mollis quam a sem iaculis euismod. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ac pharetra justo, vel dapibus tortor.</p>\r\n', 'Active'),
(9, 4, 'Project Name Here', 'www.example.net', 'John Doe', '2018-04-04', '04 April 2018', '1522781203.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer porttitor massa sed velit egestas vulputate. Morbi turpis tellus, porta in cursus at, finibus vitae dui. Nam mollis quam a sem iaculis euismod. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ac pharetra justo, vel dapibus tortor.</p>\r\n', 'Active'),
(10, 4, 'Project Name Here', 'www.example.net', 'John Doe', '2018-04-04', '04 April 2018', '1522781214.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer porttitor massa sed velit egestas vulputate. Morbi turpis tellus, porta in cursus at, finibus vitae dui. Nam mollis quam a sem iaculis euismod. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ac pharetra justo, vel dapibus tortor.</p>\r\n', 'Active'),
(11, 4, 'Project Name Here', 'www.example.net', 'John Doe', '2018-04-04', '04 April 2018', '1522781234.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer porttitor massa sed velit egestas vulputate. Morbi turpis tellus, porta in cursus at, finibus vitae dui. Nam mollis quam a sem iaculis euismod. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ac pharetra justo, vel dapibus tortor.</p>\r\n', 'Active'),
(12, 3, 'Project Name Here', 'www.example.net', 'John Doe', '2018-04-04', '04 April 2018', '1522781243.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer porttitor massa sed velit egestas vulputate. Morbi turpis tellus, porta in cursus at, finibus vitae dui. Nam mollis quam a sem iaculis euismod. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ac pharetra justo, vel dapibus tortor.</p>\r\n', 'Active'),
(17, 3, 'Project Name Here', 'www.example.net', 'John Doe', '2018-04-04', '04 April 2018', '1522781251.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer porttitor massa sed velit egestas vulputate. Morbi turpis tellus, porta in cursus at, finibus vitae dui. Nam mollis quam a sem iaculis euismod. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ac pharetra justo, vel dapibus tortor.</p>\r\n', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `service_manage`
--

CREATE TABLE IF NOT EXISTS `service_manage` (
  `se_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_features` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`se_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `service_manage`
--

INSERT INTO `service_manage` (`se_id`, `service_name`, `service_features`, `service_img`, `service_desc`, `status`) VALUES
(3, 'Web Development', ' Web Template design, Static website design, Custom web design, Responsive web design', '1522776385.png', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non vel, sint nisi possimus sunt veritatis.</p>\r\n', 'Active'),
(4, 'App Development', ' iPhone iOs Apps Development, Android Apps Development, Web Apps Development, Hybrid Apps Development', '1522776413.png', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non vel, sint nisi possimus sunt veritatis.</p>\r\n', 'Active'),
(5, 'SEO Optimization', ' Award-winning design, Email Design, Easy to Customize pages, Powerful Performance', '1522776439.png', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non vel, sint nisi possimus sunt veritatis.</p>\r\n', 'Active'),
(6, 'email marketing', 'Logo & Branding,Website Design,Video Production', '1522776553.png', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non vel, sint nisi possimus sunt veritatis.</p>\r\n', 'Active'),
(7, 'lead generation', 'Digital Strategy,Website Design,Mobile App Design,Content Writing', '1522776637.png', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non vel, sint nisi possimus sunt veritatis.</p>\r\n', 'Active'),
(8, 'Web Analysis', 'Logo & Branding,Business Consulting,Graphic/Print Design,Video Production', '1522776731.png', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non vel, sint nisi possimus sunt veritatis.</p>\r\n', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `skills_manage`
--

CREATE TABLE IF NOT EXISTS `skills_manage` (
  `sk_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `skills_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skills_count` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `skills_manage`
--

INSERT INTO `skills_manage` (`sk_id`, `skills_name`, `skills_count`, `status`) VALUES
(1, 'HTML/CSS', 90, 'Active'),
(2, 'CSS/BOOTSTRAP', 95, 'Active'),
(3, 'JAVASCRIPT', 90, 'Active'),
(4, 'PHP/CODEIGNITER', 90, 'Active'),
(5, 'JS/JQUERY', 90, 'Active'),
(6, 'AJAX', 80, 'Active'),
(7, 'PHP/LARAVEL', 90, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `slide_manage`
--

CREATE TABLE IF NOT EXISTS `slide_manage` (
  `sl_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slide_img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `slide_manage`
--

INSERT INTO `slide_manage` (`sl_id`, `heading`, `sub_heading`, `slide_img`, `status`) VALUES
(2, 'Mehediul Hassan Miton', 'Freelancer,Graphic Designer,Web Designer', '1522771074.jpg', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE IF NOT EXISTS `social` (
  `so_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`so_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`so_id`, `icon`, `url`, `status`) VALUES
(2, 'fa fa-twitter', 'https://twitter.com/', 'Active'),
(3, 'fa fa-facebook', 'http://facebook.com', 'Active'),
(4, 'fa fa-linkedin', 'https://www.linkedin.com/', 'Active'),
(5, 'fa fa-instagram', 'https://www.instagram.com/', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `stat_manage`
--

CREATE TABLE IF NOT EXISTS `stat_manage` (
  `st_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `count_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`st_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `stat_manage`
--

INSERT INTO `stat_manage` (`st_id`, `icon`, `title`, `count_number`, `status`) VALUES
(1, 'fa fa-user', 'HAPPY CLIENTS', '154', 'Active'),
(3, 'fa fa-calendar', 'YEARS OF EXPERIENCE', '1', 'Active'),
(4, 'fa fa-thumbs-up', 'PROJECTS COMPLETED', '44', 'Active'),
(5, 'fa fa-trophy', 'AWARD WON', '25', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial_manage`
--

CREATE TABLE IF NOT EXISTS `testimonial_manage` (
  `te_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`te_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `testimonial_manage`
--

INSERT INTO `testimonial_manage` (`te_id`, `name`, `designation`, `image`, `description`, `status`) VALUES
(1, 'PAUL FLAVIUS', 'Graphic Designer', '1522782686.jpg', '<p>Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tinunt ut laoreet dolore magna aliquam.</p>\r\n', 'Active'),
(2, 'PAUL FLAVIUS', 'Graphic Designer', '1522782669.jpg', '<p>Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tinunt ut laoreet dolore magna aliquam.</p>\r\n', 'Active'),
(3, 'PAUL FLAVIUS', 'Graphic Designer', '1522782721.jpg', '<p>Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tinunt ut laoreet dolore magna aliquam.</p>\r\n', 'Active'),
(4, 'PAUL FLAVIUS', 'Graphic Designer', '1522782655.jpg', '<p>Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tinunt ut laoreet dolore magna aliquam.</p>\r\n', 'Active'),
(5, 'PAUL FLAVIUS', 'Graphic Designer', '1522782640.jpg', '<p>Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tinunt ut laoreet dolore magna aliquam.</p>\r\n', 'Active'),
(6, 'PAUL FLAVIUS', 'Graphic Designer', '1522782647.jpg', '<p>Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tinunt ut laoreet dolore magna aliquam.</p>\r\n', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mehediul Hassan Miton', 'mdmiton321@gmail.com', '$2y$10$sYEZNQCQMssmnN0Ho/iMXOmGSIwPMbuDzjgzUQZY7q0t6aU6rwHlG', 'Emp3dNmMDiTP41dBxCzWDlCZSAnrju3DA875Oju8w0jXzfQeGgbIHY92yAJx', '2018-04-02 18:32:41', '2018-04-03 20:22:07');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
